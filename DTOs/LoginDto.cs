using System.ComponentModel.DataAnnotations;

namespace DeadNoteNew.DTOs
{
    public class LoginDto
    {
        [Required]
        [MaxLength(50)]
        public string Email { get; set; }

        [Required]
        [MaxLength(32)]
        public string Password { get; set; }
    }
}