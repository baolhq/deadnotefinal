using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace DeadNoteNew.DTOs
{
    public class RegisterDto
    {
        [Key]
        [Required]
        public Guid Id { get; set; }

        [Required]
        [MaxLength(50)]
        public string Email { get; set; }

        [Required]
        [MaxLength(32)]
        public string Password { get; set; }

        [Required]
        [MaxLength(32)]
        public string ConfirmPassword { get; set; }

        [Required]
        [MaxLength(50)]
        public string FullName { get; set; }

        [Required]
        [DefaultValue(false)]
        public bool IsTeacher { get; set; }

        [Required]
        [DefaultValue(false)]
        public bool IsAdmin { get; set; }

        [Required]
        public string Gender { get; set; }

        [Required]
        [DataType(DataType.Date)]
        public DateTime Birthday { get; set; }

        [Required]
        public string Address { get; set; }
    }
}