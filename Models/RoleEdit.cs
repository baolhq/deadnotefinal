using Microsoft.AspNetCore.Identity;

namespace DeadNoteNew.Models
{
    public class RoleEdit
    {
        public IdentityRole Role { get; set; }
    }
}