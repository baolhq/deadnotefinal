﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace DeadNoteNew.Models
{
    public class Account
    {
        public Account()
        {
            Classes = new HashSet<Class>();
            Deadlines = new HashSet<Deadline>();
        }

        [Key]
        [Required]
        public Guid Id { get; set; }

        [Required]
        [MaxLength(50)]
        public string Email { get; set; }

        [Required]
        [MaxLength(32)]
        public string Password { get; set; }

        [Required]
        [MaxLength(50)]
        public string FullName { get; set; }

        [Required]
        [DefaultValue(false)]
        public bool IsTeacher { get; set; }

        public bool IsAdmin { get; set; }

        public string Gender { get; set; }

        [DataType(DataType.Date)]
        public DateTime Birthday { get; set; }

        public string Address { get; set; }

        public virtual ICollection<Class> Classes { get; set; }

        public virtual ICollection<Deadline> Deadlines { get; set; }

        [DefaultValue(true)]
        public bool Status { get; set; } = true;

        [DefaultValue(false)]
        public bool ConfirmEmail { get; set; } = false;

        [DefaultValue(false)]
        public bool resetTokenStatus { get; set; } = false;
    }
}