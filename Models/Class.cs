﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace DeadNoteNew.Models
{
    public class Class
    {
        public Class()
        {
            Accounts = new HashSet<Account>();
            Deadlines = new HashSet<Deadline>();
        }
        
        [Key]
        [Required]
        public Guid Id { get; set; }

        [Required]
        public string Name { get; set; }

        [DefaultValue(true)]
        public bool Status { get; set; }

        public virtual ICollection<Account> Accounts { get; set; }

        public virtual ICollection<Deadline> Deadlines { get; set; }
    }
}