﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace DeadNoteNew.Models
{
    public class Deadline
    {
        public Deadline()
        {
            Accounts = new HashSet<Account>();
            Classes = new HashSet<Class>();
        }

        [Key]
        [Required]
        public Guid Id { get; set; }

        [Required(ErrorMessage = "{0} cannot be blank")]
        [Display(Name = "Title")]
        public string Title { get; set; }

        public string Content { get; set; }

        [Required(ErrorMessage = "{0} cannot be blank")]
        [DataType(DataType.Date)]
        public DateTime StartDate { get; set; }

        [Required(ErrorMessage = "{0} cannot be blank")]
        [DataType(DataType.Date)]
        public DateTime EndDate { get; set; }

        public virtual ICollection<Account> Accounts { get; set; }

        public virtual ICollection<Class> Classes { get; set; }

        [DefaultValue(true)]
        public bool Status { get; set; }
    }
}