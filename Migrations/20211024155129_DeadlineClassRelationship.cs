﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DeadNoteNew.Migrations
{
    public partial class DeadlineClassRelationship : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Deadlines_Classes_ClassId",
                table: "Deadlines");

            migrationBuilder.DropIndex(
                name: "IX_Deadlines_ClassId",
                table: "Deadlines");

            migrationBuilder.DropColumn(
                name: "ClassId",
                table: "Deadlines");

            migrationBuilder.CreateTable(
                name: "ClassDeadline",
                columns: table => new
                {
                    ClassesId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    DeadlinesId = table.Column<Guid>(type: "uniqueidentifier", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ClassDeadline", x => new { x.ClassesId, x.DeadlinesId });
                    table.ForeignKey(
                        name: "FK_ClassDeadline_Classes_ClassesId",
                        column: x => x.ClassesId,
                        principalTable: "Classes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ClassDeadline_Deadlines_DeadlinesId",
                        column: x => x.DeadlinesId,
                        principalTable: "Deadlines",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ClassDeadline_DeadlinesId",
                table: "ClassDeadline",
                column: "DeadlinesId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ClassDeadline");

            migrationBuilder.AddColumn<Guid>(
                name: "ClassId",
                table: "Deadlines",
                type: "uniqueidentifier",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Deadlines_ClassId",
                table: "Deadlines",
                column: "ClassId");

            migrationBuilder.AddForeignKey(
                name: "FK_Deadlines_Classes_ClassId",
                table: "Deadlines",
                column: "ClassId",
                principalTable: "Classes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
