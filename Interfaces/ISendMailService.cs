using System.Threading.Tasks;
using DeadNoteNew.Services;
using MimeKit;

namespace DeadNoteNew.Interfaces
{
    public interface ISendMailService
    {
        // Send email to single user
        Task<string> SendMailSingle(MailContent mailContent);

        // Send email to multiple user in a list
        Task<string> SendMailMultiple(MailContent mailContent, InternetAddressList list);

        // Send email asynchronously
        Task SendEmailAsync(string email, string subject, string htmlMessage);
    }
}