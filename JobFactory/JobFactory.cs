﻿using Quartz;
using Quartz.Spi;
using System;
using System.Collections.Generic;
using System.Text;

namespace DeadNoteNew.JobFactory
{
    class MyJobFactory : IJobFactory
    {
        private readonly IServiceProvider service;

        /// <summary>
        /// Initialize job factory
        /// </summary>
        /// <param name="serviceProvider"></param>
        public MyJobFactory(IServiceProvider serviceProvider)
        {
            service = serviceProvider;
        }

        /// <summary>
        /// Trigger job factory to send email after a while
        /// </summary>
        /// <param name="bundle"></param>
        /// <param name="scheduler"></param>
        /// <returns></returns>
        public IJob NewJob(TriggerFiredBundle bundle, IScheduler scheduler)
        {
            var jobDetail = bundle.JobDetail;
            return (IJob)service.GetService(jobDetail.JobType);
        }

        public void ReturnJob(IJob job)
        {
            
        }

    }
}
