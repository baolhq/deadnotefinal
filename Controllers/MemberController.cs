﻿using System;
using System.Linq;
using DeadNoteNew.Databases;
using Microsoft.AspNetCore.Mvc;
using DeadNoteNew.Models;
using System.ComponentModel;
using Microsoft.EntityFrameworkCore;
using AspNetCoreHero.ToastNotification.Abstractions;

namespace DeadNoteNew.Controllers
{
    public class MemberController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly INotyfService _notyfService;

        public MemberController(ApplicationDbContext context, INotyfService notyfService)
        {
            _notyfService = notyfService;
            _context = context;
        }

        /// <summary>
        /// Show list of member in class
        /// </summary>
        /// <param name="id">Required, ID of class to show member</param>
        /// <returns>Member in that class</returns>
        public IActionResult Index(Guid id)
        {
            var uid = Request.Cookies["uid"];
            if (!Utils.CheckGuid(uid)) return Redirect("/Login");

            var guid = Guid.Parse(uid);
            var account = _context.Accounts.Find(guid);
            if (account == null) return Redirect("/Login");

            var foundClass = _context.Classes.Find(id);
            if (foundClass == null) return Redirect("/Class");

            var members = _context.Accounts.Where(a => a.Classes.Any(c => c.Id == id));

            ViewData["foundClass"] = foundClass;
            ViewData["members"] = members;
            ViewData["isTeacher"] = account.IsTeacher;

            return View();
        }

        /// <summary>
        /// Add member to class
        /// </summary>
        /// <returns></returns>
        public IActionResult AddMember(Guid id)
        {
            TempData["classId"] = id;
            return View();
        }

        /// <summary>
        /// Callback function to add member
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public IActionResult AddMember(Guid classId, Guid memberId)
        {
            var member = _context.Accounts.FirstOrDefault(a => a.Id == memberId);
            if (member == null)
            {
                _notyfService.Error("Member not found!!");
                return View();
            }

            var foundClass = _context.Classes.SingleOrDefault(c => c.Id == classId);
            foundClass.Accounts.Add(member);
            _context.SaveChanges();
            return RedirectToAction("Index", classId);
        }

        /// <summary>
        /// Return View to confirm delete member in class
        /// </summary>
        /// <returns></returns>
        [HttpGet("Member/Delete/{classId}/{memberId}")]
        public IActionResult Delete(Guid memberId, Guid classId)
        {
            if (memberId == null || classId == null)
            {
                _notyfService.Error("Member not found!!");
                return View();
            }

            var member = _context.Accounts.SingleOrDefault(a => a.Id == memberId);
            if (member == null)
            {
                _notyfService.Error("Member not found!!");
                return View();
            }

            ViewData["classId"] = classId;

            return View(member);
        }

        /// <summary>
        /// Callback function to delete member in class
        /// </summary>
        /// <param name="memberId">Member to delete</param>
        /// <param name="classId">Class to be delete member</param>
        /// <returns></returns>
        [HttpPost("Member/Delete")]

        public IActionResult DeleteMember(string classId, Guid memberId)
        {
            // var foundClass = _context.Classes.Find(Guid.Parse(classId));

            var foundClass = this._context.Classes.Include(a => a.Accounts).SingleOrDefault(a => a.Id == Guid.Parse(classId));
            if (foundClass == null)
            {
                _notyfService.Error("Class not found!!");
                return View();
            }

            var member = _context.Accounts.Find(memberId);
            if (member == null)
            {
                _notyfService.Error("Member not found!!");
                return View();
            }

            foundClass.Accounts.Remove(member);
            _context.Update(foundClass);
            _context.SaveChanges();



            return RedirectToAction("Index");
        }
    }
}