﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using DeadNoteNew.Databases;
using DeadNoteNew.DTOs;
using DeadNoteNew.Interfaces;
using DeadNoteNew.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using MimeKit;
using System.Security.Cryptography;
using System.Threading.Tasks;
using System;
using DeadNoteNew.Services;
using Microsoft.AspNetCore.Http;
using AspNetCoreHero.ToastNotification.Abstractions;

namespace DeadNoteNew.Controllers
{
    public class RegisterController : Controller
    {
        private readonly ApplicationDbContext _db;
        private readonly UserManager<AppUser> _userManager;
        private readonly ISendMailService _sendMailService;
        public INotyfService _notyfService { get; set; }

        /// <summary>
        /// Constructor to initialize controller
        /// </summary>
        /// <param name="db">Context database</param>
        /// <param name="userManager"></param>
        /// <param name="sendMailService"></param>
        public RegisterController(ApplicationDbContext db, UserManager<AppUser> userManager, ISendMailService sendMailService, INotyfService notyfService)
        {
            _notyfService = notyfService;
            _sendMailService = sendMailService;
            _userManager = userManager;
            _db = db;
        }

        /// <summary>
        /// Show register form
        /// </summary>
        /// <returns></returns>
        public IActionResult Register()
        {
            return View();
        }

        /// <summary>
        /// Callback function after registered, if user registered successfully, redirect to ClassController
        /// </summary>
        /// <param name="registerDto"></param>
        /// <returns></returns>
        [HttpPost]
        public IActionResult Register(RegisterDto registerDto)
        {
            bool emailCheck = false;
            bool passwordCheck = false;
            bool birthdayCheck = false;

            if (ModelState.IsValid)
            {
                //check Email exist.
                var check = _db.Accounts.FirstOrDefault(c => c.Email == registerDto.Email);

                if (check != null)
                {
                    emailCheck = true;
                }
                //End check Email exist.

                //Validate Password and Password Confirmation
                // for (int i = 0; i < registerDto.Password.Length; i++)
                // {
                //     if (registerDto.Password[i] != registerDto.ConfirmPassword[i])
                //     {

                //     }
                // }

                if (registerDto.Password != registerDto.ConfirmPassword)
                {
                    passwordCheck =true;
                }

                var validators = _userManager.PasswordValidators;
                foreach (var validator in validators)
                {
                    var rs = validator.ValidateAsync(_userManager, null, registerDto.Password).Result;
                    if (!rs.Succeeded)
                    {
                        foreach (var error in rs.Errors)
                        {
                            // TempData["StatusMessage"] = TempData["StatusMessage"] + error.Description;
                            _notyfService.Error(error.Description);
                        }
                    }
                }
                // End Validate Password and Password Confirmation

                // Validate Birthday
                var today = DateTime.Today;

                if (registerDto.Birthday > today)
                {   
                    birthdayCheck = true;
                }

                if (emailCheck || passwordCheck || birthdayCheck)
                {
                    
                    if (emailCheck) _notyfService.Warning("Account Exitsed");
                    if (passwordCheck)_notyfService.Error("Password must match");
                    if (birthdayCheck)_notyfService.Error("Invalid datetime");
                    return View();
                }

                // End Validate Birthday

                //Using Identity Framework to create users and confirmation email
                AppUser user = new AppUser()
                {
                    UserName = registerDto.FullName,
                    Email = registerDto.Email
                };

                IdentityResult result = _userManager.CreateAsync(user, registerDto.Password).Result;

                //if result success, continue
                if (result.Succeeded)
                {
                    //Generate confirmation email token
                    string confirmationToken = _userManager.GenerateEmailConfirmationTokenAsync(user).Result;
                    string confirmationLink = Url.Action("ConfirmEmail", "Register",
                    new
                    {
                        email = user.Email,
                        token = confirmationToken
                    }, protocol: HttpContext.Request.Scheme);
                    // End Generate confirmation email token

                    // Send confirmation email to user's mail
                    var mailContent = new MailContent();
                    mailContent.To = registerDto.Email;
                    mailContent.Subject = "Xac Thuc Dang Ky";
                    mailContent.Body = confirmationLink;
                    var rs = _sendMailService.SendMailSingle(mailContent);
                    // End send confirmation email to user's mail

                    // Add user to database
                    var account = new Account
                    {
                        Id = registerDto.Id,
                        Email = registerDto.Email,
                        Password = Utils.GetMD5Hash(registerDto.Password),
                        FullName = registerDto.FullName,
                        IsTeacher = registerDto.IsTeacher,
                        IsAdmin = registerDto.IsAdmin,
                        Gender = registerDto.Gender,
                        Birthday = registerDto.Birthday,
                        Address = registerDto.Address
                    };

                    this._db.Accounts.Add(account);
                    this._db.SaveChanges();
                }
            }
            return View();
        }

        /// <summary>
        /// Allow user to confirm their email, check if email is activated, if so, show error
        /// </summary>
        /// <param name="email"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task<IActionResult> ConfirmEmail(string email, string token)
        {
            //Check email exist
            var user = await _userManager.FindByEmailAsync(email);
            if (user == null) return View("NotConfirm");

            //check email confirmed, reuturn "Email da duoc xac thuc" if confirmed
            if (user.EmailConfirmed == true)
            {
                ViewData["IsUsed"] = true;
            }
            else
            {
                ViewData["IsUsed"] = false;
            }

            // Validates that an email confirmation token matches the specified user
            var result = await _userManager.ConfirmEmailAsync(user, token);
            if (result.Succeeded)
            {
                var account = _db.Accounts.SingleOrDefault(c => c.Email == email);
                account.ConfirmEmail = true;
                _db.SaveChanges();
            }

            return View(result.Succeeded ? "ConfirmEmail" : "NotConfirm");

        }
        public IActionResult Error()
        {
            return View();
        }
    }
}