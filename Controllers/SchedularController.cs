using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using DeadNoteNew.Databases;
using DeadNoteNew.Interfaces;
using DeadNoteNew.Models;
using DeadNoteNew.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using MimeKit;
using Quartz;
using Quartz.Spi;
//using deadnotenew.Models;

namespace deadnotenew.Controllers
{
    public class SchedularController : Controller
    {
        public IScheduler Scheduler { get; set; }
        private readonly IJobFactory jobFactory;
        private readonly List<JobMetadata> jobMetadatas;
        private readonly ISchedulerFactory schedulerFactory;
        private readonly IServiceScopeFactory serviceScopeFactory;
        private readonly ApplicationDbContext _db;

        /// <summary>
        /// Constructor with 5 parameters to initialize SchedularController
        /// </summary>
        /// <param name="db">Database</param>
        /// <param name="jobFactory"></param>
        /// <param name="jobMetadatas"></param>
        /// <param name="schedulerFactory"></param>
        /// <param name="serviceScopeFactory"></param>
        public SchedularController(ApplicationDbContext db, IJobFactory jobFactory, List<JobMetadata> jobMetadatas, ISchedulerFactory schedulerFactory, IServiceScopeFactory serviceScopeFactory)
        {
            _db = db;
            this.jobFactory = jobFactory;
            this.jobMetadatas = jobMetadatas;
            this.schedulerFactory = schedulerFactory;
            this.serviceScopeFactory = serviceScopeFactory;
        }

        /// <summary>
        /// Show schedular view
        /// </summary>
        /// <returns></returns>
        public IActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// Start the schedule to send email to after 2 days
        /// </summary>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<IActionResult> StartSchedule(CancellationToken cancellationToken)
        {
            // Creating Schdeular
            Scheduler = await schedulerFactory.GetScheduler();
            Scheduler.JobFactory = jobFactory;

            //Suporrt for Multiple Jobs
            jobMetadatas?.ForEach(jobMetadata =>
            {
                //Create Job
                IJobDetail jobDetail = CreateJob(jobMetadata);
                //Create trigger
                ITrigger trigger = CreateTrigger(jobMetadata);
                //Schedule Job
                Scheduler.ScheduleJob(jobDetail, trigger, cancellationToken).GetAwaiter();
                //Start The Schedular
            });
            await Scheduler.Start(cancellationToken);

            return View();
        }


        /// <summary>
        /// Create job trigger to send mail
        /// </summary>
        /// <param name="jobMetadata"></param>
        /// <returns></returns>
        private ITrigger CreateTrigger(JobMetadata jobMetadata)
        {
            DateTime EndDate = Convert.ToDateTime(HttpContext.Session.GetString("Deadline"));
            string endTime = (HttpContext.Session.GetString("EndTime"));
            // Date start notificate
            var startD = CheckDate(EndDate);
            // Date end notificate
            var endD = EndDate.AddDays(-1);

            var endH = Convert.ToInt32(endTime.Split(":")[0]);
            var endM = Convert.ToInt32(endTime.Split(":")[1]);


            // The notification will start at startD and end at endD, the notification will repeat everyday at 3:22 until endD
            return TriggerBuilder.Create()
            .WithIdentity(jobMetadata.JobId.ToString())
            .WithDescription(jobMetadata.JobName)
            .StartAt(startD)
            .WithDailyTimeIntervalSchedule(s =>
            s.OnEveryDay().WithIntervalInHours(24).StartingDailyAt(TimeOfDay.HourAndMinuteOfDay(endH,endM)))
            .EndAt(endD)
            .Build();
        }

        /// <summary>
        /// /// Create job to send deadline email
        /// </summary>
        /// <param name="jobMetadata"></param>
        /// <returns></returns>
        private IJobDetail CreateJob(JobMetadata jobMetadata)
        {
            return JobBuilder.Create(jobMetadata.JobType)
                .WithIdentity(jobMetadata.JobId.ToString())
                .WithDescription(jobMetadata.JobName)
                .Build();
        }

        /// <summary>
        /// Stop sending deadline asynchronously
        /// </summary>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<IActionResult> StopAsync(CancellationToken cancellationToken)
        {
            await Scheduler.Shutdown();
            return View("StartSchedule");
        }

        /// <summary>
        /// Check if the date to send deadline is over
        /// </summary>
        /// <param name="Dead">DateTime when to stop</param>
        /// <returns></returns>
        public DateTime CheckDate(DateTime Dead)
        {
            var DeadDay = Dead.Day - 2;
            var Mon = Dead.Month;
            var Year = Dead.Year;
            if (DeadDay == 0)
            {
                if (Mon == 1)
                {
                    Year -= 1;
                    Mon = 12;
                    DeadDay = 31;
                }
                else if (Mon == 2)
                {
                    Mon -= 1;
                    DeadDay = 31;
                }
                else if (Mon == 3)
                {
                    Mon -= 2;
                    DeadDay = 28;
                }
                else if (Mon == 8)
                {
                    Mon -= 1;
                    DeadDay = 31;
                }
                else if (Mon == 5 || Mon == 7 || Mon == 10 || Mon == 12)
                {
                    Mon -= 1;
                    DeadDay = 30;
                }
                else if (Mon == 4 || Mon == 6 || Mon == 9 || Mon == 11)
                {
                    Mon -= 1;
                    DeadDay = 31;
                }
            }
            else if (DeadDay == -1)
            {
                if (Mon == 1)
                {
                    Year -= 1;
                    Mon = 12;
                    DeadDay = 30;
                }
                else if (Mon == 2)
                {
                    Mon -= 1;
                    DeadDay = 30;
                }
                else if (Mon == 3)
                {
                    Mon -= 2;
                    DeadDay = 27;
                }
                else if (Mon == 8)
                {
                    Mon -= 1;
                    DeadDay = 30;
                }
                else if (Mon == 5 || Mon == 7 || Mon == 10 || Mon == 12)
                {
                    Mon -= 1;
                    DeadDay = 29;
                }
                else if (Mon == 4 || Mon == 6 || Mon == 9 || Mon == 11)
                {
                    Mon -= 1;
                    DeadDay = 30;
                }
            }

            DateTime dd = new DateTime(Year, Mon, DeadDay);

            return dd;
        }

        /// <summary>
        /// Subclass to send notification
        /// </summary>
        public class NotificationJob : IJob
        {
            private readonly ILogger<NotificationJob> _logger;
            private readonly ISendMailService _sendMailService;
            private readonly IServiceProvider _service;
            private readonly IHttpContextAccessor _httpContextAccessor;

            /// <summary>
            /// Constructor to initialize NotificationJob
            /// </summary>
            /// <param name="logger"></param>
            /// <param name="sendMailService"></param>
            /// <param name="service"></param>
            /// <param name="httpContextAccessor"></param>
            public NotificationJob(ILogger<NotificationJob> logger, ISendMailService sendMailService, IServiceProvider service, IHttpContextAccessor httpContextAccessor)
            {
                _httpContextAccessor = httpContextAccessor;
                _service = service;
                _sendMailService = sendMailService;
                this._logger = logger;
            }

            /// <summary>
            /// Sending email from template to list of student
            /// </summary>
            /// <param name="context"></param>
            /// <returns></returns>
            public Task Execute(IJobExecutionContext context)
            {
                using (var scope = _service.CreateScope())
                {
         
                    var db = scope.ServiceProvider.GetService<ApplicationDbContext>();
                    InternetAddressList list = new InternetAddressList();

                    var builder = new BodyBuilder();
                    //Giao thuc IO Truyen file
                    using (StreamReader SourceReader = System.IO.File.OpenText(@"C:\Users\Thanh\Downloads\Compressed\deadnotenew-master\deadnotenew-master\Controllers\Templates\index.html"))
                    {
                        builder.HtmlBody = SourceReader.ReadToEnd();
                    }
                    // replace chữ trong indexs
                   
                    string htmlBody = builder.HtmlBody.Replace("PARTY", "Notification DeadLine")
                    .Replace("Start date : date1", "Today is: " + DateTime.Now.ToShortDateString())
                    .Replace("End date : date2", "Still time to complete your deadline")
                    .Replace("Placeholder1", "Please check your DEADLINE NOTIFICATION, it's coming close!!!")
                    .Replace("Placeholder2", "\"Your time is limited, so don’t waste it living someone else’s life\" <br> - <i>Steve Jobs</i>");
                    string messagebody = string.Format("{0}", htmlBody);

                    var emailList = db.Accounts;

                    foreach (var item in emailList)
                    {
                        if(item.IsTeacher){
                            continue;
                        }
                        list.Add(new MailboxAddress(item.Email, item.Email));
                    }

                    var mailContent = new MailContent();
                    // mailContent.To = "nguyenthan1844@gmail.com";
                    mailContent.Subject = "Your Deadline is comingggggg!!!";
                    mailContent.Body = messagebody;
                    var rs = _sendMailService.SendMailMultiple(mailContent, list);

                    _logger.LogInformation($"Notification Job: Notify User at {DateTime.Now} and Jobtype: {context.JobDetail.JobType}");
                    return Task.CompletedTask;
                }
            }
        }
    }

}