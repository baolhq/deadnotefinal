﻿using System;
using DeadNoteNew.Databases;
using Microsoft.AspNetCore.Mvc;

namespace DeadNoteNew.Controllers
{
    public class JoinController : Controller
    {
        private readonly ApplicationDbContext _context;

        public JoinController(ApplicationDbContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Show form to take user input
        /// </summary>
        /// <returns></returns>
        public IActionResult Index()
        {
            var uid = Request.Cookies["uid"];
            if (!Utils.CheckGuid(uid)) return Redirect("/Login");

            var guid = Guid.Parse(uid);
            var account = _context.Accounts.Find(guid);

            if (account == null) return Redirect("/Register");
            return View();
        }
        

        /// <summary>
        /// Callback function to join class, if user joined that class, redirect to Class controller,
        ///  else create a link between user and that class
        /// </summary>
        /// <param name="classId"></param>
        /// <returns></returns>
        [HttpPost]
        public IActionResult Index(string classId)
        {
            if (string.IsNullOrEmpty(classId)) return NotFound();

            var parsedId = Guid.Parse(classId);
            var foundClass = _context.Classes.Find(parsedId);

            if (foundClass == null) return NotFound();

            var uid = Request.Cookies["uid"];
            if (string.IsNullOrEmpty(uid))
            {
                return Redirect("/Login");
            }

            var guid = Guid.Parse(uid);
            var account = _context.Accounts.Find(guid);
            if (account == null) return Redirect("/Register");

            // User already in that class
            if (foundClass.Accounts.Contains(account)) return Redirect("/Class");

            foundClass.Accounts.Add(account);
            _context.SaveChanges();
            return Redirect("/Class");
        }
    }
}