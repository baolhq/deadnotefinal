﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using AspNetCoreHero.ToastNotification.Abstractions;
using DeadNoteNew.Databases;
using DeadNoteNew.Interfaces;
using DeadNoteNew.Models;
using DeadNoteNew.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MimeKit;

namespace DeadNoteNew.Controllers
{
    public class DeadlineController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly ISendMailService _sendMailService;

        /// <summary>
        /// Initialize controller
        /// </summary>
        /// <param name="context"></param>
        /// <param name="sendMailService"></param>
        
        //Notif FUNCTion
        private readonly IHttpContextAccessor _httpContextAccessor;
        public INotyfService _notyfService { get; set; }
        public DeadlineController(ApplicationDbContext context, ISendMailService sendMailService, IHttpContextAccessor httpContextAccessor, INotyfService notyfService)
        {
            _notyfService = notyfService;
            _httpContextAccessor = httpContextAccessor;
            _context = context;
            _sendMailService = sendMailService;
        }

        /// <summary>
        /// Return all deadline of all class that user joined in
        /// </summary>
        /// <param name="sortOrder"></param>
        /// <param name="currentFilter"></param>
        /// <param name="searchString"></param>
        /// <param name="pageNumber"></param>
        /// <returns></returns>
        public async Task<IActionResult> Index(string sortOrder, string currentFilter, string searchString, int? pageNumber)
        {

            var uid = Request.Cookies["uid"];

            if (string.IsNullOrEmpty(uid))
            {
                return Redirect("/Login");
            }

            var guid = Guid.Parse(uid);
            var account = _context.Accounts.Find(guid);
            ViewData["account"] = account;

            if (account == null) return Redirect("/Login");

            ViewData["CurrentSort"] = sortOrder;
            
            if (searchString != null)
            {
                pageNumber = 1;
            }
            else
            {
                searchString = currentFilter;
            }
            IEnumerable<Deadline> DeadlineList = this._context.Deadlines;
            var obj = _context.Deadlines.Where(s => s.Status == true);
            int pageSize = 3;
            return View(await PaginatedList<Deadline>.CreateAsync(obj.AsNoTracking(), pageNumber ?? 1, pageSize));

            // return View(obj);
        }

        /// <summary>
        /// View all deadlines in specific class
        /// </summary>
        /// <param name="id">ID of the class user want to see deadlines</param>
        /// <returns></returns>
        public async Task<IActionResult>  Class(string id,string sortOrder, string currentFilter, string searchString, int? pageNumber)
        {
            if (string.IsNullOrEmpty(id)) return NotFound();

            ViewData["classID"] = id;


            var uid = Request.Cookies["uid"];
            if (!Utils.CheckGuid(uid)) return Redirect("/Login");

            var guid = Guid.Parse(uid);
            var account = _context.Accounts.Find(guid);

            var foundClass = _context.Classes.Find(Guid.Parse(id));
            ViewData["className"] = foundClass.Name;
            ViewData["account"] = account;

            // var result = _context.Classes.Where(c => c.Id == Guid.Parse(id)).SelectMany(c => c.Deadlines);
             ViewData["CurrentSort"] = sortOrder;
            
            if (searchString != null)
            {
                pageNumber = 1;
            }
            else
            {
                searchString = currentFilter;
            }
            IEnumerable<Deadline> DeadlineList = this._context.Deadlines;
            
            var result = _context.Classes.Where(c => c.Id == Guid.Parse(id)).SelectMany(c => c.Deadlines);
            int pageSize = 3;
            return View( await PaginatedList<Deadline>.CreateAsync(result.AsNoTracking(), pageNumber ?? 1, pageSize));
         }

        /// <summary>
        /// Allow teacher to create deadline
        /// </summary>
        /// <returns></returns>
        public IActionResult Create(string id)
        {
            if (string.IsNullOrEmpty(id)) return NotFound();

            ViewData["classID"] = id;
            return View();
        }

        /// <summary>
        /// Callback function to create deadline
        /// /// </summary>
        /// <param name="obj"></param>
        /// <param name="StartTime"></param>
        /// <param name="StartDate"></param>
        /// <param name="EndTime"></param>
        /// <param name="EndDate"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Obsolete]
        public IActionResult Create(Deadline obj, string StartTime, DateTime StartDate, string EndTime, DateTime EndDate, string classID)
        {
            if (ModelState.IsValid)
            {

                var today = DateTime.Now;
                var startDayError = false;
                var endDayError = false;
                var compareError = false;
                if (StartDate < today)
                {
                    startDayError = true;
                    _notyfService.Error("Start date can not less than today!!");
                }
                if(EndDate < today)
                {
                    endDayError = true;
                    _notyfService.Error("Start date can not less than today!!");
                } 
                if(StartDate > EndDate){
                    compareError = true;
                    _notyfService.Error("Start date can not less than today!!");
                }

                if(startDayError || endDayError || compareError)
                {
                    ViewBag.title1= obj.Title;
                    ViewBag.content= obj.Content;
                    ViewBag.classId= obj.Id;
                    ViewBag.startTime = StartTime;
                    ViewBag.startDate = StartDate;
                    ViewBag.endTime = EndTime;
                    ViewBag.endDate = EndDate;
                    return View(obj);
                }
                

                obj.Status = true;
                DateTime s = StartDate;
                var time = StartTime.Split(":");
                TimeSpan ts = new TimeSpan(int.Parse(time[0]), int.Parse(time[1]), 0);
                s = s.Date + ts;
                obj.StartDate = s;

                DateTime s1 = EndDate;
                var time1 = EndTime.Split(":");
                TimeSpan ts1 = new TimeSpan(int.Parse(time1[0]), int.Parse(time1[1]), 0);
                s1 = s1.Date + ts1;
                obj.EndDate = s1;

                //Set session
                HttpContext.Session.SetString("Deadline", obj.EndDate.ToString());
                HttpContext.Session.SetString("EndTime", EndTime);
               
                var builder = new BodyBuilder();
                //Giao thuc IO Truyen file
                using (StreamReader SourceReader = System.IO.File.OpenText(@"C:\Users\Thanh\Downloads\Compressed\deadnotenew-master\deadnotenew-master\Controllers\Templates\index.html"))
                {
                    builder.HtmlBody = SourceReader.ReadToEnd();
                }
                // replace chữ trong index
                string htmlBody = builder.HtmlBody.Replace("PARTY", obj.Title)
                .Replace("date1", obj.StartDate.ToString())
                .Replace("date2", obj.EndDate.ToString())
                .Replace("Placeholder1", obj.Content)
                .Replace("Placeholder2", "\"Your time is limited, so don’t waste it living someone else’s life\" <br> - <i>Steve Jobs</i>");
                string messagebody = string.Format("{0}", htmlBody);

                //List ra tat ca mail hien co trong table Account
                InternetAddressList list = new InternetAddressList();
                var emailList = this._context.Accounts;
                foreach (var item in emailList)
                {
                    list.Add(new MailboxAddress(item.Email));
                }

                var mailContent = new MailContent();
                // mailContent.To = "LongHBCE150048@fpt.edu.vn";
                mailContent.Subject = "DEADLINE NOFIFICATION";
                mailContent.Body = messagebody;
                var rs = _sendMailService.SendMailMultiple(mailContent, list);

                //Cong nghe moi
                _notyfService.Success("DeadLine has been added");

                TempData["IsUsed"] = true;

                this._context.Deadlines.Add(obj);
                if (string.IsNullOrEmpty(classID)) return NotFound();
                var parsedId = Guid.Parse(classID);

                var foundClass = _context.Classes.Find(parsedId);
                if (foundClass == null) return NotFound();

                obj.Id = Guid.NewGuid();

                foundClass.Deadlines.Add(obj);
                this._context.SaveChanges();

                return Redirect("/Deadline/Class/"+ classID);
            }
            return View(obj);
        }

        /// <summary>
        /// Get deadline info
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public IActionResult Update(Guid id)
        {
            var obj = this._context.Deadlines.Find(id);
            if (obj == null)
            {
                return NotFound();
            }
            ViewData["obj"] = obj;
            return View(obj);
        }

        /// <summary>
        /// Update deadline information
        /// </summary>
        /// <param name="id"></param>
        /// <param name="Title"></param>
        /// <param name="Content"></param>
        /// <param name="StartTime"></param>
        /// <param name="StartDate"></param>
        /// <param name="EndTime"></param>
        /// <param name="EndDate"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Update(Guid id, string Title, string Content, string StartTime, DateTime StartDate, string EndTime, DateTime EndDate)
        {
            var obj = this._context.Deadlines.Find(id);
            if (obj == null)
            {
                return NotFound();
            }
            if (Title == null || Content == null)
            {
                return View(obj);
            }
            obj.Title = Title;
            obj.Content = Content;
            DateTime s = StartDate;
            var time = StartTime.Split(":");
            TimeSpan ts = new TimeSpan(int.Parse(time[0]), int.Parse(time[1]), 0);
            s = s.Date + ts;
            obj.StartDate = s;

            DateTime s1 = EndDate;
            var time1 = EndTime.Split(":");
            TimeSpan ts1 = new TimeSpan(int.Parse(time1[0]), int.Parse(time1[1]), 0);
            s1 = s1.Date + ts1;
            obj.EndDate = s1;
            _notyfService.Success("Deadline has been update");
            this._context.Deadlines.Update(obj);
            this._context.SaveChanges();
            return RedirectToAction("Index");
        }


        /// <summary>
        /// Get deadline info
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public IActionResult Delete(Guid id)
        {
            var obj = this._context.Deadlines.Find(id);
            if (obj == null)
            {
                return NotFound();
            }
            return View(obj);
        }

        /// <summary>
        /// Delete deadline with specific id, only teacher can access
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Delete(Guid? Id)
        {
            var item = _context.Deadlines.Find(Id);
            // var obj = this._db.Categories.Find(cat_id);
            if (item == null)
            {
                return NotFound();
            }
            item.Status = false;
            IEnumerable<Deadline> deadlines1 = this._context.Deadlines;
            var deadlines = deadlines1.Select(p => p).Where(p => p.Id == Id);
            foreach (var obj in deadlines)
            {
                obj.Status = false;
            }
            _notyfService.Success("Deadline has been delete");
            this._context.SaveChanges();
            return RedirectToAction("Index", "Class");
        }
    }
}