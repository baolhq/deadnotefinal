﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DeadNoteNew.DTOs;
using DeadNoteNew.Interfaces;
using DeadNoteNew.Databases;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Security.Cryptography;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using DeadNoteNew.Services;
using MimeKit;
using DeadNoteNew.Models;
using AspNetCoreHero.ToastNotification.Abstractions;
//using DeadNote01.Models;

namespace DeadNoteNew.Controllers
{
    public class LoginController : Controller
    {
        private readonly ApplicationDbContext _db;
        private readonly UserManager<AppUser> _userManager;
        private readonly ISendMailService _sendMailService;

        public INotyfService _notyfService { get; set; }
        

        /// <summary>
        /// Initialize login controller
        /// </summary>
        /// <param name="db"></param>
        /// <param name="userManager"></param>
        /// <param name="sendMailService"></param>
        public LoginController(ApplicationDbContext db, UserManager<AppUser> userManager, ISendMailService sendMailService,INotyfService notyfService)
        {   
            _notyfService = notyfService;
            _sendMailService = sendMailService;
            _userManager = userManager;
            _db = db;
        }

        /// <summary>
        /// Return Index View for login
        /// </summary>
        /// <returns></returns>
        public IActionResult Index()
        {
            return View();
        }
        

        /// <summary>
        /// Callback function to login, check if model valid and validate token before submit
        /// </summary>
        /// <param name="loginDto"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult<AccountDto>> Index(LoginDto loginDto)
        {
            if (ModelState.IsValid)
            {
                //xem them ve singleordefaultasync
                var account = await _db.Accounts.SingleOrDefaultAsync(x => x.Email == loginDto.Email);

                if (account == null) {
                    _notyfService.Error("Invalid email or password");
                    return View();
                }

                // IF account weren't confirmed, return to Error view
                if (account.ConfirmEmail == false) return RedirectToAction("Error");

                // MD5 hash 
                var computeHash = Utils.GetMD5Hash(loginDto.Password);

                // check that password which is user entered mathces with password that saved in database
                for (int i = 0; i < computeHash.Length; i++)
                {
                    if (computeHash[i] != account.Password[i])
                    {
                        _notyfService.Error("Invalid email or password");
                        return View();
                    }
                }

                //set the key value in Cookie  
                Set("uid", account.Id.ToString(), 24 * 60);
                HttpContext.Session.SetString("loggedIn", "yes");
                _notyfService.Success("Login Success");
                return RedirectToAction("Index", "Class");
            }
            return View(loginDto);

        }

        /// <summary>
        /// Logout user from system
        /// </summary>
        public IActionResult Logout()
        {
            //Delete the cookie object  
            Remove("uid");
            HttpContext.Session.Remove("loggedIn");
            return RedirectToAction("Index");
        }

        /// <summary>
        /// Set cookie key, value and expire time
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        /// <param name="expireTime"></param>
        public void Set(string key, string value, int? expireTime)
        {
            CookieOptions option = new CookieOptions();
            if (expireTime.HasValue)
                option.Expires = DateTime.Now.AddHours(expireTime.Value);
            else
                option.Expires = DateTime.Now.AddMilliseconds(10);
            Response.Cookies.Append(key, value, option);
        }

        /// <summary>
        /// Remove user cookie
        /// </summary>
        /// <param name="key"></param>
        public void Remove(string key)
        {
            //Sets an expired cookie
            Response.Cookies.Delete(key);
        }

        /// <summary>
        /// Send reset password link to user email
        /// </summary>
        /// <returns></returns>
        public IActionResult SendPasswordResetLink()
        {
            return View();
        }

        /// <summary>
        /// Callback funtion for SendPasswordResetLink, after user enter their credentical
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public IActionResult SendPasswordResetLink(string email)
        {
            // Find email that exist or not
            AppUser user = _userManager.FindByEmailAsync(email).Result;

            // If it doesn't exist or not confirmed, return "Error while resetting your password!"
            if (user == null || !(user.EmailConfirmed))
            {
                _notyfService.Error("Email does not exsist");
                return View();
            }

            var account = this._db.Accounts.SingleOrDefault(x => x.Email == email);
            account.resetTokenStatus = false;
            _db.SaveChanges();

            //Generates a password reset token for the specified user, using the configured password reset token provider.
            var token = _userManager.GeneratePasswordResetTokenAsync(user).Result;

            //Generates a URL with a path for an action method, which contains the specified action name, 
            //controller name, route values, and protocol to use. See the remarks section for important security information.
            var resetLink = Url.Action("ResetPassword", "Login",
            new
            {
                email = user.Email,
                token = token
            }, protocol: HttpContext.Request.Scheme);

            // Send reset password link to user
            var mailContent = new MailContent();
            mailContent.To = email;
            mailContent.Subject = "RESET PASSWORD LINK!";
            mailContent.Body = resetLink;
            var rs = _sendMailService.SendMailSingle(mailContent);
            _notyfService.Success("Please Check Your Email To Verify Reset Password Link");
            return View("Index");

        }

        /// <summary>
        /// Reset password field
        /// </summary>
        /// <param name="token"></param>
        /// <param name="email"></param>
        /// <returns></returns>
        [HttpGet]
        [AllowAnonymous]
        public IActionResult ResetPassword(string token, string email)
        {
            // If password reset token or email is null, most likely the
            // user tried to tamper the password reset link
            var account = this._db.Accounts.SingleOrDefault(c => c.Email == email);

            if (account.resetTokenStatus == true)
            {
                ViewData["IsUsed"] = true;
            }
            else
            {
                ViewData["IsUsed"] = false;
            }
            if (token == null || email == null)
            {
                ModelState.AddModelError("", "Invalid password reset token");
            }
            return View();
        }

        /// <summary>
        /// Callback function for ResetPasswordField
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> ResetPassword(ResetPasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                // Find the user by email
                var user = await _userManager.FindByEmailAsync(model.Email);
                var account = this._db.Accounts.SingleOrDefault(c => c.Email == model.Email);
                if (user != null)
                {
                    if (model.Password == model.ConfirmPassword)
                    {
                        // reset the user password
                        var result = await _userManager.ResetPasswordAsync(user, model.Token, model.Password);
                        if (result.Succeeded)
                        {
                            account.Password = Utils.GetMD5Hash(model.Password);
                            account.resetTokenStatus = true;
                            this._db.SaveChanges();
                            return View("ResetPasswordConfirmation");
                        }
                        // Display validation errors. For example, password reset token already
                        // used to change the password or password complexity rules not met
                        foreach (var error in result.Errors)
                        {
                            ModelState.AddModelError("", error.Description);
                        }
                        return View(model);
                    }
                    _notyfService.Error("Password Confirm Must Match!!");

                    return View(model);
                }

                // To avoid account enumeration and brute force attacks, don't
                // reveal that the user does not exist
                return View("Error");
            }
            // Display validation errors if model state is not valid
            return View(model);
        }
    }
}