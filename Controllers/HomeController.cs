﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using DeadNoteNew.Models;

namespace DeadNoteNew.Controllers
{
    public class HomeController : Controller
    {
        /// <summary>
        /// Return Index page, store uid in tempdata
        /// </summary>
        /// <returns></returns>
        public IActionResult Index()
        {
            var uid = Request.Cookies["uid"];
            TempData["uid"] = uid;
            return View();
        }

        /// <summary>
        /// About page
        /// </summary>
        /// <returns></returns>
        public IActionResult About()
        {
            return View();
        }
        
    }
}