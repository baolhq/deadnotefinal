using System;
using System.Collections.Generic;
using System.Security.Authentication;
using System.Threading.Tasks;
using API.Services;
using DeadNoteNew.Interfaces;
using DeadNoteNew.Models;
using MailKit.Security;
using Microsoft.Extensions.Options;
using MimeKit;

namespace DeadNoteNew.Services
{
    public class SendMailService : ISendMailService
    {
        MailSettings _mailSetting { set; get; }

        public SendMailService(IOptions<MailSettings> mailSetting)
        {
            _mailSetting = mailSetting.Value;

        }
        public async Task<string> SendMailSingle(MailContent mailContent)
        {
            var email = new MimeMessage();
            email.Sender = new MailboxAddress(_mailSetting.DisplayName, _mailSetting.Mail);
            email.From.Add(new MailboxAddress(_mailSetting.DisplayName, _mailSetting.Mail));

            email.To.Add(new MailboxAddress(mailContent.To, mailContent.To));

            email.Subject = mailContent.Subject;

            var builder = new BodyBuilder();

            builder.HtmlBody = mailContent.Body;

            email.Body = builder.ToMessageBody();

            using var smtp = new MailKit.Net.Smtp.SmtpClient();

            try
            {
                

                await smtp.ConnectAsync(_mailSetting.Host, _mailSetting.Port, SecureSocketOptions.StartTls);
                await smtp.AuthenticateAsync(_mailSetting.Mail, _mailSetting.Password);
                await smtp.SendAsync(email);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return "Error" + e.Message;
            }

            smtp.Disconnect(true);
            return "SEND SUCCESS";
        }

        public async Task SendEmailAsync(string email, string subject, string htmlMessage)
        {
            await SendMailSingle(new MailContent()
            {
                To = email,
                Subject = subject,
                Body = htmlMessage
            });
        }

        public async Task<string> SendMailMultiple(MailContent mailContent, InternetAddressList list)
        {
            var email = new MimeMessage();
            email.Sender = new MailboxAddress(_mailSetting.DisplayName, _mailSetting.Mail);
            email.From.Add(new MailboxAddress(_mailSetting.DisplayName, _mailSetting.Mail));
            email.To.AddRange(list);
            email.Subject = mailContent.Subject;

            var builder = new BodyBuilder();

            builder.HtmlBody = mailContent.Body;

            email.Body = builder.ToMessageBody();

            using var smtp = new MailKit.Net.Smtp.SmtpClient();

            try
            {
                await smtp.ConnectAsync(_mailSetting.Host, _mailSetting.Port, SecureSocketOptions.StartTls);
                await smtp.AuthenticateAsync(_mailSetting.Mail, _mailSetting.Password);
                await smtp.SendAsync(email);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return "Error" + e.Message;
            }

            smtp.Disconnect(true);
            return "SEND SUCCESS";
        }
    }

    public class MailContent
    {
        public string To { get; set; }

        public string Subject { get; set; }

        public string Body { get; set; }
    }
}