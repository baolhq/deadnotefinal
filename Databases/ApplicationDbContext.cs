﻿using DeadNoteNew.Models;
using Microsoft.EntityFrameworkCore;

namespace DeadNoteNew.Databases
{
    public class ApplicationDbContext : DbContext
    {
        /// <summary>
        /// Use database context options, initialize database
        /// </summary>
        /// <param name="options"></param>
        /// <returns></returns>
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        {
        }

        public DbSet<Account> Accounts { get; set; }
        public DbSet<Deadline> Deadlines { get; set; }
        public DbSet<Class> Classes { get; set; }

        public DbSet<AppUser> AppUsers { get; set; }
    }
}