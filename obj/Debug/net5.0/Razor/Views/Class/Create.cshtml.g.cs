#pragma checksum "C:\Users\Thanh\Downloads\Compressed\deadnotenew-master\deadnotenew-master\Views\Class\Create.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "6ae3144449d097f489575fee21bce5c937600ae4"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Class_Create), @"mvc.1.0.view", @"/Views/Class/Create.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "C:\Users\Thanh\Downloads\Compressed\deadnotenew-master\deadnotenew-master\Views\_ViewImports.cshtml"
using DeadNoteNew;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\Users\Thanh\Downloads\Compressed\deadnotenew-master\deadnotenew-master\Views\_ViewImports.cshtml"
using DeadNoteNew.Models;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"6ae3144449d097f489575fee21bce5c937600ae4", @"/Views/Class/Create.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"dc9074ff369dcb36bd6d12a742dc1ed6f631bbdf", @"/Views/_ViewImports.cshtml")]
    public class Views_Class_Create : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<Class>
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-action", "Create", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_1 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("method", "POST", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        #pragma warning restore 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.FormTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper;
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.RenderAtEndOfFormTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            WriteLiteral("\n");
#nullable restore
#line 3 "C:\Users\Thanh\Downloads\Compressed\deadnotenew-master\deadnotenew-master\Views\Class\Create.cshtml"
  

    ViewData["Title"] = "Create Class";

    var classNameExist = ViewData["classNameExist"];

#line default
#line hidden
#nullable disable
            WriteLiteral("\n<h1 class=\"text-center\">Create new class</h1>\n\n");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("form", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "6ae3144449d097f489575fee21bce5c937600ae44237", async() => {
                WriteLiteral(@"
    <div class=""row justify-content-center"">

        <div class=""form-group col-8"">
            <label for=""subject"">Subject: </label>
            <input type=""text"" class=""form-control"" id=""subject"" name=""subject"" placeholder=""Ex: SWP391"" required>
        </div>
        <div class=""form-group col-8 mt-3"">
            <label for=""student-group"">Student group: </label>
            <input type=""text"" class=""form-control"" id=""student-group"" name=""studentGroup"" placeholder=""Ex: SE1504""
                required>
        </div>

    </div>

");
#nullable restore
#line 27 "C:\Users\Thanh\Downloads\Compressed\deadnotenew-master\deadnotenew-master\Views\Class\Create.cshtml"
     if (classNameExist != null)

    {

#line default
#line hidden
#nullable disable
                WriteLiteral("        <p class=\"text-center text-danger mt-3\">");
#nullable restore
#line 30 "C:\Users\Thanh\Downloads\Compressed\deadnotenew-master\deadnotenew-master\Views\Class\Create.cshtml"
                                           Write(classNameExist);

#line default
#line hidden
#nullable disable
                WriteLiteral("</p>\n");
#nullable restore
#line 31 "C:\Users\Thanh\Downloads\Compressed\deadnotenew-master\deadnotenew-master\Views\Class\Create.cshtml"

    }

#line default
#line hidden
#nullable disable
                WriteLiteral("\n    <div class=\"row justify-content-center mt-3\">\n        <input class=\"btn col-sm-5 btn-primary\" type=\"submit\" value=\"Create\">\n    </div>\n");
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.FormTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.RenderAtEndOfFormTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper.Action = (string)__tagHelperAttribute_0.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_0);
            __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper.Method = (string)__tagHelperAttribute_1.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_1);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<Class> Html { get; private set; }
    }
}
#pragma warning restore 1591
