#pragma checksum "C:\Users\Thanh\Downloads\Compressed\deadnotenew-master\deadnotenew-master\Views\DeadlineBackup\Index.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "37ad694e648e11851382c8adfe42cf704ca5b6e2"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_DeadlineBackup_Index), @"mvc.1.0.view", @"/Views/DeadlineBackup/Index.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "C:\Users\Thanh\Downloads\Compressed\deadnotenew-master\deadnotenew-master\Views\_ViewImports.cshtml"
using DeadNoteNew;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\Users\Thanh\Downloads\Compressed\deadnotenew-master\deadnotenew-master\Views\_ViewImports.cshtml"
using DeadNoteNew.Models;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"37ad694e648e11851382c8adfe42cf704ca5b6e2", @"/Views/DeadlineBackup/Index.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"dc9074ff369dcb36bd6d12a742dc1ed6f631bbdf", @"/Views/_ViewImports.cshtml")]
    public class Views_DeadlineBackup_Index : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<IQueryable<Deadline>>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            WriteLiteral("\n");
#nullable restore
#line 3 "C:\Users\Thanh\Downloads\Compressed\deadnotenew-master\deadnotenew-master\Views\DeadlineBackup\Index.cshtml"
  
    ViewData["Title"] = "Deadline";

#line default
#line hidden
#nullable disable
            WriteLiteral(@"
<table class=""table table-bordered table-hover table-striped"">
    <caption class=""text-center"">All your deadlines</caption>
    <thead class=""thead-dark"">
    <th>ID</th>
    <th>Title</th>
    <th>Content</th>
    <th>StartDate</th>
    <th>EndDate</th>
    </thead>

    <tbody>
");
#nullable restore
#line 18 "C:\Users\Thanh\Downloads\Compressed\deadnotenew-master\deadnotenew-master\Views\DeadlineBackup\Index.cshtml"
         foreach (var item in Model)
        {

#line default
#line hidden
#nullable disable
            WriteLiteral("            <tr>\n                <td>");
#nullable restore
#line 21 "C:\Users\Thanh\Downloads\Compressed\deadnotenew-master\deadnotenew-master\Views\DeadlineBackup\Index.cshtml"
               Write(item.Id);

#line default
#line hidden
#nullable disable
            WriteLiteral("</td>\n                <td>");
#nullable restore
#line 22 "C:\Users\Thanh\Downloads\Compressed\deadnotenew-master\deadnotenew-master\Views\DeadlineBackup\Index.cshtml"
               Write(item.Title);

#line default
#line hidden
#nullable disable
            WriteLiteral("</td>\n                <td>");
#nullable restore
#line 23 "C:\Users\Thanh\Downloads\Compressed\deadnotenew-master\deadnotenew-master\Views\DeadlineBackup\Index.cshtml"
               Write(item.Content);

#line default
#line hidden
#nullable disable
            WriteLiteral("</td>\n                <td>");
#nullable restore
#line 24 "C:\Users\Thanh\Downloads\Compressed\deadnotenew-master\deadnotenew-master\Views\DeadlineBackup\Index.cshtml"
               Write(item.StartDate);

#line default
#line hidden
#nullable disable
            WriteLiteral("</td>\n                <td>");
#nullable restore
#line 25 "C:\Users\Thanh\Downloads\Compressed\deadnotenew-master\deadnotenew-master\Views\DeadlineBackup\Index.cshtml"
               Write(item.EndDate);

#line default
#line hidden
#nullable disable
            WriteLiteral("</td>\n            </tr>\n");
#nullable restore
#line 27 "C:\Users\Thanh\Downloads\Compressed\deadnotenew-master\deadnotenew-master\Views\DeadlineBackup\Index.cshtml"
        }

#line default
#line hidden
#nullable disable
            WriteLiteral("    </tbody>\n</table>");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<IQueryable<Deadline>> Html { get; private set; }
    }
}
#pragma warning restore 1591
