# Application Development Project: DeadNote System

## Project description

The **DeadNote** is a system that takes and manages important notes throughout the lecture for both students and lecturers, setting up daily deadlines notifications through emails and notification. The system aim to be a easy to use, convenience and able to keep users to stay on their schedules.

## Screenshots

![Home screen](https://i.imgur.com/apemGpp.png)
_Simple by design, extends when needed_

![Teamwork](https://i.imgur.com/L5w9TI6.png)
_Teamwork with LiveShare extension_

## Group 3 - Contributors:

- Lê Hoàng Quốc Bảo - CE150509
- Nguyễn Tường Thành - CE150463
- Lý Nguyễn Hoàng Phúc - CE150373
- Phạm Minh Nhí - CE150093
- Trần Minh Nguyệt - CE150549
- Nguyễn Khánh Hưng - CE130351
