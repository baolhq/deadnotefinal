﻿using System;
using System.Security.Cryptography;
using System.Text;
namespace DeadNoteNew
{
    public static class Utils
    {
        /// <summary>
        /// Check if Guid is valid or not, also trying to parse it to check
        /// </summary>
        /// <param name="id">Guid in string type</param>
        /// <returns>Whether id is a valid Guid</returns>
        public static bool CheckGuid(string id)
        {
            if (string.IsNullOrEmpty(id)) return false;
            return Guid.TryParse(id, out _);
        }

        /// <summary>
        /// MD5 Hashing user's password
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static string GetMD5Hash(string str)
        {
            //MD5 algorithm
            MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider();
            byte[] bytes = ASCIIEncoding.Default.GetBytes(str);
            byte[] encoded = md5.ComputeHash(bytes);

            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < encoded.Length; i++)
                sb.Append(encoded[i].ToString("x2"));

            return sb.ToString();
        }
    }
}